import React,{useEffect} from "react";
import { AdvanceTablesWidget7, ListsWidget12, ListsWidget14 } from '../../../_metronic/_partials/widgets';
import { shallowEqual, useSelector } from "react-redux";
import { notify2 } from "../../config/Toastify";
// import { toAbsoluteUrl } from "../../../_metronic/_helpers";


export function ProfileOverview() {
  const user = useSelector(({ auth }) => auth.user, shallowEqual);

  useEffect(() => {
    return () => {};
  }, [user]);

  console.log(user)

  return (
    <div className="row">
      {notify2()}
      <div className="col-lg-6">
        <ListsWidget14 className="card-stretch gutter-b"></ListsWidget14>
      </div>
      <div className="col-lg-6">
        <ListsWidget12 className="card-stretch gutter-b"></ListsWidget12>
      </div>
      <div className="col-lg-12">
        <AdvanceTablesWidget7 className="card-stretch gutter-b"></AdvanceTablesWidget7>
      </div>
    </div>
  );
}
