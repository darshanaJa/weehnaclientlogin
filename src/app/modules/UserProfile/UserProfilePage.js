import React from "react";
import { 
  // Redirect, 
  Route, Switch } from "react-router-dom";
// import { useSubheader } from "../../../_metronic/layout";
import AccountInformation from "./AccountInformation";
import { ProfileOverview } from "./ProfileOverview";
import { Products } from "./Products";
import ChangePassword from "./ChangePassword";
import PersonaInformation from "./PersonaInformation";
import EmailSettings from "./EmailSettings";
import { ProfileCard } from "./components/ProfileCard";
import eCommercePage from "../ECommerce/pages/eCommercePage";
// eCommer
// eCommercePage

export default function UserProfilePage() {
  // const suhbeader = useSubheader();
  // suhbeader.setTitle("Dashboard");

  return (
    <div className="d-flex flex-row">
      <ProfileCard></ProfileCard>
      <div className="flex-row-fluid ml-lg-8">
        <Switch>
          {/* <Redirect
            from="/personal-information"
            exact={true}
            to="/dashboard"
          /> */}
          <Route
            path="/dashboard" exact
            component={ProfileOverview}
          />
          <Route
            path="/account-information"
            component={AccountInformation}
          />
          <Route
            path="/change-password"
            component={ChangePassword}
          />
          <Route
            path="/dashboard/email-settings"
            component={EmailSettings}
          />
          <Route
            path="/personal-information"
            component={PersonaInformation}
          />
          <Route
            path="/sales/payments"
            component={eCommercePage}
          />
          <Route
            path="/sales/requests"
            component={eCommercePage}
          />
          <Route
            path="/sales/history"
            component={eCommercePage}
          />
          <Route
            path="/sales/products"
            component={Products}
          />
        </Switch>
      </div>
    </div>
  );
}
