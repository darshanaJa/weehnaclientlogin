// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {
  Input,
  Select,
  DatePickerField,
} from "../../../../../../_metronic/_partials/controls";

// Validation schema
// const CustomerEditSchema = Yup.object().shape({
//   firstName: Yup.string()
//     .min(3, "Minimum 3 symbols")
//     .max(50, "Maximum 50 symbols")
//     .required("Firstname is required"),
//   lastName: Yup.string()
//     .min(3, "Minimum 3 symbols")
//     .max(50, "Maximum 50 symbols")
//     .required("Lastname is required"),
//   email: Yup.string()
//     .email("Invalid email")
//     .required("Email is required"),
//   userName: Yup.string().required("Username is required"),
//   dateOfBbirth: Yup.mixed()
//     .nullable(false)
//     .required("Date of Birth is required"),
//   ipAddress: Yup.string().required("IP Address is required"),
// });

export function CustomerEditForm({
  onHide,
}) {
  return (
        <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {/* <h1>aa<h1> */}
             </Modal.Body> 
             
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              <button
                type="submit"
                // onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button>
            </Modal.Footer>
      </>
  );
}
