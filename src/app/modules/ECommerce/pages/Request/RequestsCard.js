import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { RequestsFilter } from "./Requests-filter/RequestsFilter";
import { RequestsTable } from "./Requests-table/RequestsTable";
import { RequestsGrouping } from "./Requests-grouping/RequestsGrouping";
import { useRequestsUIContext } from "./RequestsUIContext";
import { notify } from "../../../config/Toastify";

export function RequestsCard() {
  const RequestsUIContext = useRequestsUIContext();
  const RequestsUIProps = useMemo(() => {
    return {
      ids: RequestsUIContext.ids,
      queryParams: RequestsUIContext.queryParams,
      setQueryParams: RequestsUIContext.setQueryParams,
      newRequestButtonClick: RequestsUIContext.newRequestButtonClick,
      openDeleteRequestsDialog: RequestsUIContext.openDeleteRequestsDialog,
      openEditRequestPage: RequestsUIContext.openEditRequestPage,
      openUpdateRequestsStatusDialog:
        RequestsUIContext.openUpdateRequestsStatusDialog,
      openFetchRequestsDialog: RequestsUIContext.openFetchRequestsDialog,
    };
  }, [RequestsUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="Price Request list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={RequestsUIProps.newRequestButtonClick}
          >
            New Request
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <RequestsFilter />
        {RequestsUIProps.ids.length > 0 && (
          <>
            <RequestsGrouping />
          </>
        )}
        <RequestsTable />
      </CardBody>
    </Card>
  );
}
