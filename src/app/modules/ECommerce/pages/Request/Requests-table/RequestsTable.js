// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Requests/RequestsActions";
import * as uiHelpers from "../RequestsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../../_metronic/_partials/controls";
import { useRequestsUIContext } from "../RequestsUIContext";

export function RequestsTable() {
  // Requests UI Context
  const RequestsUIContext = useRequestsUIContext();
  const RequestsUIProps = useMemo(() => {
    return {
      ids: RequestsUIContext.ids,
      setIds: RequestsUIContext.setIds,
      queryParams: RequestsUIContext.queryParams,
      setQueryParams: RequestsUIContext.setQueryParams,
      openEditRequestPage: RequestsUIContext.openEditRequestPage,
      openDeleteRequestDialog: RequestsUIContext.openDeleteRequestDialog,
    };
  }, [RequestsUIContext]);

  // Getting curret state of Requests list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Requests }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Requests Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    RequestsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchRequests(RequestsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [RequestsUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    {
      dataField: "id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "description",
      text: "Description",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "sale_rquest_registered_date",
      text: "Request Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "sale_rquest_updated_date",
      text: "Update Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditRequestPage: RequestsUIProps.openEditRequestPage,
        openDeleteRequestDialog: RequestsUIProps.openDeleteRequestDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: RequestsUIProps.queryParams.pageSize,
    page: RequestsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  RequestsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: RequestsUIProps.ids,
                  setIds: RequestsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
