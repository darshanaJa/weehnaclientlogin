import React from "react";
import {
  RequestConditionCssClasses,
  RequestConditionTitles
} from "../../RequestsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        RequestConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        RequestConditionCssClasses[row.condition]
      }`}
    >
      {RequestConditionTitles[row.condition]}
    </span>
  </>
);
