import React from "react";
import { Route } from "react-router-dom";
import { RequestsLoadingDialog } from "./Requests-loading-dialog/RequestsLoadingDialog";
import { RequestDeleteDialog } from "./Request-delete-dialog/RequestDeleteDialog";
import { RequestsDeleteDialog } from "./Requests-delete-dialog/RequestsDeleteDialog";
import { RequestsFetchDialog } from "./Requests-fetch-dialog/RequestsFetchDialog";
import { RequestsUpdateStatusDialog } from "./Requests-update-status-dialog/RequestsUpdateStatusDialog";
import { RequestsCard } from "./RequestsCard";
import { RequestsUIProvider } from "./RequestsUIContext";

export function RequestsPage({ history }) {
  const RequestsUIEvents = {
    newRequestButtonClick: () => {
      history.push("/sales/requests/new");
    },
    openEditRequestPage: (id) => {
      history.push(`/sales/requests/${id}/edit`);
    },
    openDeleteRequestDialog: (id) => {
      history.push(`/sales/requests/${id}/delete`);
    },
    openDeleteRequestsDialog: () => {
      history.push(`/sales/requests/deleteRequests`);
    },
    openFetchRequestsDialog: () => {
      history.push(`/sales/requests/fetch`);
    },
    openUpdateRequestsStatusDialog: () => {
      history.push("/sales/requests/updateStatus");
    },
  };

  return (
    <RequestsUIProvider RequestsUIEvents={RequestsUIEvents}>
      <RequestsLoadingDialog />
      <Route path="/sales/requests/deleteRequests">
        {({ history, match }) => (
          <RequestsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/requests");
            }}
          />
        )}
      </Route>
      <Route path="/sales/requests/:id/delete">
        {({ history, match }) => (
          <RequestDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/sales/requests");
            }}
          />
        )}
      </Route>
      <Route path="/sales/requests/fetch">
        {({ history, match }) => (
          <RequestsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/requests");
            }}
          />
        )}
      </Route>
      <Route path="/sales/requests/updateStatus">
        {({ history, match }) => (
          <RequestsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/requests");
            }}
          />
        )}
      </Route>
      <RequestsCard />
    </RequestsUIProvider>
  );
}
