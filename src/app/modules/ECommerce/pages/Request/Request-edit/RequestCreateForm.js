import React, {useState, useEffect, useReducer} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../../../_metronic/_partials/controls";
// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   RequeststatusTitles,
//   RequestConditionTitles,
// } from "../RequestsUIHelpers";
import "../../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import axios from "axios";
// import { Shops_URL_GET } from "../../../_redux/shops/shopsCrud";
// import { MainStocks_URL_GET } from "../../../_redux/MainStock/MainStocksCrud";
// import { getSalesTicketById, SalesTickets_URL_FIND } from "../../../_redux/salesTickets/salesTicketsCrud";
// import { RequestForm } from "./RequestForm";
// import { getSpecificationById } from "../../../_redux/specificationsItem/specificationsCrud";
// import { admin_token } from "../../../../config/config";
import { Api_Login } from "../../../../config/config";
// import { bindActionCreators } from "redux";
// import { push } from "object-pasth";



const RequestEditSchema = Yup.object().shape({
  
});

export function RequestCreateForm({
  Request,
  btnRef,
  saveRequest,
  todos,
  setTodos,
  setSaleId,
  saleId,
  setComment,
  comment,
  storeId,
  setStoreId
}) {

  // console.log(Request)
 

  // const [shop,setShop]=useState([]);
  const [shop2,setShop2]=useState([]);
  // const [saleticketId,setSaleTicketdId]=useState([]);
  // const [saleItemId,setSaleItemId]=useState([]);
  // const [saleItId,setSaleItId]=useState([]);

  const [saleItem,setSaleItem]=useState([]);
  const [saleItemQuntity,setSaleItemQuntity]=useState();
  // const [saleItemPrice,setSaleItemPrice]=useState([]);
  // const [stockName,setStockName]=useState([]);

  const itemInitialState = {
    items1 : [],
    count:0,
  }

  // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjUxMDJjZWRjLThmYmEtNGFlZi1hODJkLTMzZTk3ZjliOWEwZiIsImZuYW1lIjoiYWJjZCIsImVtYWlsIjoidGVzdEBnbWFpbC5jb20iLCJyb2xlIjoiY3VzdG9tZXIiLCJleHAiOjE2MzAxMjU5OTQuODQxLCJpYXQiOjE2MjQ5NDE5OTR9.bB9oHZ67I-XrI-RcUGFGO-HWRhWRdsrizG1-mMVT21g
  
  // const stockName = shop2.filter(el=>el.item_id===saleItem)
  // console.log(stockName[0].item_name)

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Api_Login + `/api/main-stock/retailstore/search`,
      data:{
        "filter": [ {"status" : 1}],
      }
      })
      .then((res) => {
        setShop2(res.data.data.results)
        console.log(res.data.data.data.results);
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  const itemReducer =(state, action)=> {
    if(action.type === 'add'){

        const updateItems = state.items1.concat(action.item);
        console.log(action.item)
        console.log(state.items1)
        console.log(updateItems)

      return {items1 : updateItems};
    }
    if(action.type === 'remove'){
      const itId=action.item_id
      console.log(itId)
      const upItem = state.items1.filter((el) => el.item_name !== itId)

      return {items1 : upItem};
    }
    return itemInitialState;
  }
  const [itemState, itemDispatch] = useReducer(itemReducer, itemInitialState)

  console.log(itemState.items1);
  setTodos(itemState.items1)


const stockNameHandler=(e)=>{
    setSaleItem(e.target.value)
  }

  const quntityHandler=(e)=>{
    setSaleItemQuntity(e.target.value)
}

  const storeIdHandler =(e)=>{
    setStoreId(e.target.value)
  }

  const CommentHandler =(e)=>{
    setComment(e.target.value)
  }

const submitTodoHandler = (e) =>{

  e.preventDefault();

  const Items4= ({ 
    item_name:saleItem, 
    quantity:+saleItemQuntity, 
  })
  
  itemDispatch({type:'add', item:Items4})
}


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Request}
        validationSchema={RequestEditSchema}
        onSubmit={(values) => {
          console.log(values);
          saveRequest(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4" >
                  <Field
                    type="date"
                    name="request_date"
                    component={Input}
                    placeholder="Request Date"
                    label="Request Date"
                    value={storeId} 
                    onChange={storeIdHandler}
                  />
                </div>
                <div className="col-lg-8" >
                  <Field
                    type="string"
                    name="description"
                    component={Input}
                    placeholder="Descripion"
                    label="Descripion"
                    value={comment} 
                    onChange={CommentHandler}
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Select name="mstockItemId"
                    error="mstockItemId"
                    label="Item Name"
                    onChange={stockNameHandler}
                    value={saleItem}
                  >
                      <option>Choose One</option>
                      {shop2.map((item) => (
                          <option value={item.item_name} >
                            {item.item_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                  <Field
                    type="number"
                    name="qty"
                    value={saleItemQuntity}
                    component={Input}
                    placeholder="Quntity"
                    label="Quntity"
                    onChange={quntityHandler}
                  />
                </div>
                <div className="col-lg-4">
                  <button type="submit" onClick={submitTodoHandler} className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                </div>
              </div>

              {itemState.items1.map((item) => (
                <div className="form-group row">
                  <div className="col-lg-4">
                    <Field
                      type="string"
                      name="qty"
                      value={item.item_name}
                      component={Input}
                      placeholder="Item Name"
                      label="Item Name"
                      onChange={quntityHandler}
                    />
                  </div>
                  <div className="col-lg-4">
                    <Field
                      type="string"
                      name="price"
                      value={item.quantity}
                      component={Input}
                      placeholder="Quantity"
                      label="Quantity"
                      // onChange={priceHandler}
                    />
                  </div>
                  <div className="col-lg-4">
                    <button type="button" 
                        onClick={()=> {itemDispatch({type:'remove', item_id:item.item_name})}}
                        className="btnform + downbtn" ><i class="fa fa-trash"></i></button>
                 </div> 
                </div>
                ))}

              {/* <RequestForm saleItId={saleItId} setSaleItId={setSaleItId} saleItemId={saleItemId} setSaleItemId={setSaleItemId} todos={todos} setTodos={setTodos} saleId={saleId}  />         */}
                <div className="form-group row">
                  <div className="col-lg-4">
                  <button type="submit" className="btn btn-primary ml-2" >Save</button>
                 </div>  
                </div> 
              
            
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
