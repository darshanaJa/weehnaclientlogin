/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Requests/RequestsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../../_metronic/_partials/controls";
import { RequestCreateForm } from "./RequestCreateForm";
import { RequestEditForm } from "./RequestEditForm";
import { Specifications } from "../Request-specifications/Specifications";
import { SpecificationsUIProvider } from "../Request-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Request-remarks/RemarksUIContext";
import { Remarks } from "../Request-remarks/Remarks";
import { getRequestById } from "../../../_redux/Requests/RequestsCrud";
import moment from 'moment';

// const initRequest = {
  // store_name: "",
  // store_province: "",
  // store_district: "",
  // store_city: "",
  // store_address: "",
  // store_cid: "",
  // store_email: "",
  // store_phone_1: "",
  // store_phone_2: "",
  // store_owner_name: "",
  // store_owner_nic: "",
  // store_owner_mobile_1: "",
  // store_owner_mobile_2: "",
  // store_owner_email: "",
  // store_image:"",
  // store_br_no:""
// }



export function RequestEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const[todos,setTodos] = useState([]);
  const [saleId,setSaleId]=useState([]);
  const [comment,setComment]=useState([]);
  const [storeId,setStoreId]=useState([]);

  const initRequest = {
    request_date:storeId,
    description:comment,
    sale_requst_items:todos,
  }

  console.log(todos)

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, RequestForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Requests.actionsLoading,
      RequestForEdit: state.Requests.RequestForEdit,
    }),
    shallowEqual
  );
  useEffect(() => {
    dispatch(actions.fetchRequest(id));
  }, [id, dispatch]);

  useEffect(() => {
    getRequestById(id)
    .then((res)=>{
      // setStoreId(res.data.request_date)
      setStoreId(moment(res.data.request_date).format("yyyy-MM-DD"))
      setComment(res.data.description)
      // itemDispatch({type:'add', item:res.data.sale_requst_items})
    })
    
  }, [id])

  useEffect(() => {
    let _title = id ? "" : "New Request";
    if (RequestForEdit && id) {
      // _title = `Edit Store '${RequestForEdit.store_name}  - ${RequestForEdit.store_phone_1}'`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [RequestForEdit, id]);

  const saveRequest = (values) => {
    if (!id) {
      dispatch(actions.createRequest(values)).then(() => backToRequestsList());
    }
     else {
      dispatch(actions.updateRequest(values,id)).then(() => backToRequestsList());
    }
  };

  // console.log(id)

  // notify();


  const btnRef = useRef(); 

  const backToRequestsList = () => {
    history.push(`/sales/requests`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToRequestsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
           {
               <li className="nav-item" onClick={() => setTab("basic")}>
               <a
                 className={`nav-link ${tab === "basic" && "active"}`}
                 data-toggle="tab"
                 role="tab"
                 aria-selected={(tab === "basic").toString()}
               >
                 Basic info
               </a>
             </li>
            }
            {/* {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Request remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Request specifications
                  </a>
                </li>
              </>
            )} */}
          </ul>
          <div className="mt-5">
            {
  
            }
            {tab === "basic" && (
              <RequestEditForm
                actionsLoading={actionsLoading}
                Request={initRequest}
                btnRef={btnRef}
                saveRequest={saveRequest}
                todos={todos}
                setTodos={setTodos}
                setSaleId={setSaleId}
                saleId={saleId}
                comment={comment}
                setComment={setComment}
                storeId={storeId}
                setStoreId={setStoreId}
                id={id}
            />
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToRequestsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
           {
               <li className="nav-item" onClick={() => setTab("basic")}>
               <a
                 className={`nav-link ${tab === "basic" && "active"}`}
                 data-toggle="tab"
                 role="tab"
                 aria-selected={(tab === "basic").toString()}
               >
                 Basic info
               </a>
             </li>
            }
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Request remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Request specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {
  
            }
            {tab === "basic" && (
              <RequestCreateForm
                actionsLoading={actionsLoading}
                Request={ RequestForEdit || initRequest}
                btnRef={btnRef}
                saveRequest={saveRequest}
                todos={todos}
                setTodos={setTodos}
                setSaleId={setSaleId}
                saleId={saleId}
                comment={comment}
                setComment={setComment}
                storeId={storeId}
                setStoreId={setStoreId}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentRequestId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentRequestId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  

  
}
