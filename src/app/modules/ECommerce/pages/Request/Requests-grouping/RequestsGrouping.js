import React, { useMemo } from "react";
import { useRequestsUIContext } from "../RequestsUIContext";

export function RequestsGrouping() {
  // Requests UI Context
  const RequestsUIContext = useRequestsUIContext();
  const RequestsUIProps = useMemo(() => {
    return {
      ids: RequestsUIContext.ids,
      setIds: RequestsUIContext.setIds,
      openDeleteRequestsDialog: RequestsUIContext.openDeleteRequestsDialog,
      openFetchRequestsDialog: RequestsUIContext.openFetchRequestsDialog,
      openUpdateRequestsStatusDialog:
        RequestsUIContext.openUpdateRequestsStatusDialog,
    };
  }, [RequestsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{RequestsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={RequestsUIProps.openDeleteRequestsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={RequestsUIProps.openFetchRequestsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={RequestsUIProps.openUpdateRequestsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
