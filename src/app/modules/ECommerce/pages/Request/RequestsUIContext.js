import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./RequestsUIHelpers";

const RequestsUIContext = createContext();

export function useRequestsUIContext() {
  return useContext(RequestsUIContext);
}

export const RequestsUIConsumer = RequestsUIContext.Consumer;

export function RequestsUIProvider({ RequestsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newRequestButtonClick: RequestsUIEvents.newRequestButtonClick,
    openEditRequestPage: RequestsUIEvents.openEditRequestPage,
    openDeleteRequestDialog: RequestsUIEvents.openDeleteRequestDialog,
    openDeleteRequestsDialog: RequestsUIEvents.openDeleteRequestsDialog,
    openFetchRequestsDialog: RequestsUIEvents.openFetchRequestsDialog,
    openUpdateRequestsStatusDialog:
      RequestsUIEvents.openUpdateRequestsStatusDialog,
  };

  return (
    <RequestsUIContext.Provider value={value}>
      {children}
    </RequestsUIContext.Provider>
  );
}
