import React, { Suspense } from "react";
import {
  //  Redirect,
   Switch } from "react-router-dom";
// import { CustomersPage } from "./customers/CustomersPage";
// import { ProductsPage } from "./products/ProductsPage";
// import { ProductEdit } from "./products/product-edit/ProductEdit";
import { LayoutSplashScreen, ContentRoute } from "../../../../_metronic/layout";
import { RequestsPage } from "./Request/RequestsPage";
import { RequestEdit } from "./Request/Request-edit/RequestEdit";
import { CreditsPage } from "../../../pages/pages/RetailSale/CreditsPage";
import { CreditEdit } from "../../../pages/pages/RetailSale/Credit-edit/CreditEdit";
import { CustomersPage } from "../../../pages/pages/customers/CustomersPage";
import { CustomerEditDialog } from "../../../pages/pages/customers/customer-edit-dialog/CustomerEditDialog";



export default function eCommercePage() { 
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>

        <ContentRoute path="/sales/requests/new" component={RequestEdit} />
        <ContentRoute path="/sales/requests/:id/edit" component={RequestEdit} />
        <ContentRoute path="/sales/requests" component={RequestsPage} />

        <ContentRoute path="/sales/history/:id/edit" component={CreditEdit} />
        <ContentRoute path="/sales/history" component={CreditsPage} />

        <ContentRoute path="/sales/payments/:id/edit" component={CustomerEditDialog} />
        <ContentRoute path="/sales/payments" component={CustomersPage} />
      </Switch>
    </Suspense>
  );
}
