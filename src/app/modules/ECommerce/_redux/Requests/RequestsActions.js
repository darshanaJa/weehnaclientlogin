import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./RequestsCrud";
import {RequestsSlice, callTypes} from "./RequestsSlice";

// notify
const {actions} = RequestsSlice;

export const fetchRequests = queryParams => dispatch => {
  console.log(queryParams);
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findRequests(queryParams)
    .then((res) => {
      let data = res.data;
      console.log(data)
      dispatch(actions.RequestsFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));
      // dispatch(actions.RequestsFetched({ totalCount:  3, entities: data }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Requests";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchRequest = id => dispatch => {
  if (!id) {
    return dispatch(actions.RequestFetched({ RequestForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getRequestById(id)
      .then((res) => {
        //notify()
      let Request = res.data; 
      console.log(Request)
      dispatch(actions.RequestFetched({ RequestForEdit: Request }));
      })
    .catch(error => {
      error.clientMessage = "Can't find Request";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteRequest = id => dispatch => {
  console.log(id)
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteRequest(id)
    .then(res => {
      dispatch(actions.RequestDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete Request";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      
    });
};

export const aprovedRequest = id => dispatch => {
  console.log(id)
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .aprovedRequest(id)
    .then(res => {
      dispatch(actions.RequestAproved({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete Request";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      
    });
};

export const rejectRequest = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .rejectedRequest(id)
    .then(res => {
      dispatch(actions.RequestRejeccted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete Request";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      
    });
};

export const createRequest = RequestForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));

  return requestFromServer

    .createRequest(RequestForCreation)
      .then((res) => {
        let Request = res.data;
        console.log(Request);
        dispatch(actions.RequestCreated({ Request })); 
        let msg = res.data.message;
        console.log(msg)
        notify(msg)
    })
    .catch(error => {
      // alert("Can't create Request");
      error.clientMessage = "Can't create Request";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateRequest = (Request,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateRequest(Request,id)
    // .then(() => {
    //   dispatch(actions.RequestUpdated({ Request }));
    .then((res) => {
      let Request = res.data;
      dispatch(actions.RequestUpdated({ Request })); 
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      //notify();
      
      
    })
    .catch(error => {
      //notify();
      error.clientMessage = "Can't update Request";
      // alert("Can't update Request");
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

// notifyError

export const updateRequestsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForRequests(ids, status)
    .then(() => {
      dispatch(actions.RequestsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Requests status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteRequests = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteRequests(ids)
    .then(() => {
      dispatch(actions.RequestsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Requests";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
