import React from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar';
import moment from 'moment';
// import ProgressBar from "bootstrap-progress-bar";

const Pr=({day1,createDate})=> {

    var date1 = new Date(moment(createDate).format("MM/DD/yyyy"));

    console.log(day1,createDate)

    console.log(moment(createDate).format("MM/DD/yyyy"))
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    var date2 = today;  

    today = mm + '/' + dd + '/' + yyyy;
    // document.write(today);

    var Difference_In_Time = date2.getTime() - date1.getTime();
    var Difference_In_Days = parseInt(Difference_In_Time / (1000 * 3600 * 24));

    const day = Difference_In_Days + parseInt(day1);

    const dayCal = day/45*100;

    return(
        <div>
         {/* <p>{Difference_In_Days}</p>
         <p>{today}</p> */}
        <ProgressBar 
            style={{
            // width:"550px", 
            height: "35px"}}
            animated now={dayCal}
            label={<h6>{`${day} Days`}</h6>} 
         />
         </div>
    );
}

export default Pr;

// export const notify = (msg) => {
//     // toast.success(msg)
//     // console.log(msg)
//         setTimeout(() => {
//           toast.success(msg);
//         }, 1200);
  
//       //  window.location.reload()
//           return (
//             <div>
//               {/* <button onClick={notify}>Notify!</button> */}
//               <ToastContainer />
//             </div>
//           );
//   }