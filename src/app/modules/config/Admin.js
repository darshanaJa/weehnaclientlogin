import React, {useState, useEffect} from "react";
import { Api_Login2 } from "./config";
import axios from 'axios';
import moment from 'moment';

const Admin =({adminId, createDate, updateDate})=> {

    const [name, setname] = useState('')

    console.log(adminId)


    useEffect(()=>{
        axios({
          method: 'get',
          baseURL: Api_Login2 + `/${adminId}`,
          })
          .then((res) => {
            console.log(res.data.Admin)
            setname(res.data.Admin)
          })
          .catch(function (response) {
              // console.log(response);
          });
          
      },[adminId])

      return(
          <>
            <div className="form-group row">
                <div className="col-lg-12">
                <hr class="new3" />
                </div>  
            </div>
                <p>Created by - <b>{name.fname + ' ' + name.lname}</b></p>
                <p>Creaed Date - <b>{(moment(createDate).format("yyyy-MMM-DD hh:mm:ss"))}</b></p>
                <p>Updated Date - <b>{(moment(updateDate).format("yyyy-MMM-DD hh:mm:ss"))}</b></p>
        </>
      )

}
export default Admin ;