import { notify,notifyError } from "../../../config/Toastify";
// import { MainStocksCard } from "../../pages/mainStock/MainStocksCard";
import * as requestFromServer from "./MainStocksCrud";
import {MainStocksSlice, callTypes} from "./MainStocksSlice";

const {actions} = MainStocksSlice;

export const fetchMainStocks = queryParams => dispatch => {
  console.log(queryParams);
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
  // console.log(queryParams)
    .findMainStocks(queryParams)

    .then((res) => {
      let data = res.data;
      dispatch(actions.MainStocksFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));
    })
    .catch(error => {
      console.log("Error Axios")
      error.clientMessage = "Can't find MainStocks";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchMainStock = id => dispatch => {
  if (!id) {
    return dispatch(actions.MainStockFetched({ MainStockForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getMainStockById(id)
    .then(response => {
      const MainStock = response.data;
      dispatch(actions.MainStockFetched({ MainStockForEdit: MainStock }));
    })
    .catch(error => {
      error.clientMessage = "Can't find MainStock";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteMainStock = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteMainStock(id)
    .then(res => {
      dispatch(actions.MainStockDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
     

    })
    .catch(error => {
      error.clientMessage = "Can't delete MainStock";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      console.log("cant Delete")
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createMainStock = MainStockForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createMainStock(MainStockForCreation)
    
    .then((res) => {
      let MainStock = res.data;
      console.log(MainStock);
      dispatch(actions.MainStockCreated({ MainStock }));
      let msg = res.data.message;
      notify(msg)
      const errormsg = res.data.data.error_message ? res.data.data.error_message: null;
      console.log(errormsg)
      notifyError(errormsg)
    
      // dispatch(actions.SalesTicketUpdated({ SalesTicket }));
    })

    .catch(error => {
      error.clientMessage = "Can't create MainStock";
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateMainStock = MainStock => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateMainStock(MainStock)
    .then((res) => {
      dispatch(actions.MainStockUpdated({ MainStock }));
      let msg = res.data.message;
      console.log(msg)
      // MainStocksCard(msg)
      notify((msg))
    })
    .catch(error => {
      error.clientMessage = "Can't update MainStock";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateMainStocksStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForMainStocks(ids, status)
    .then(() => {
      dispatch(actions.MainStocksStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update MainStocks status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteMainStocks = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteMainStocks(ids)
    .then(() => {
      dispatch(actions.MainStocksDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete MainStocks";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
