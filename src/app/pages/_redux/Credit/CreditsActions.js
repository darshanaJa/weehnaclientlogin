import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./CreditsCrud";
import {CreditsSlice, callTypes} from "./CreditsSlice";

const {actions} = CreditsSlice;

export const fetchCredits = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findCredits(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.CreditsFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Credits";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchCredit = id => dispatch => {
  if (!id) {
    return dispatch(actions.CreditFetched({ CreditForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getCreditById(id)
    .then((res) => {
      let Credit = res.data; 
      dispatch(actions.CreditFetched({ CreditForEdit: Credit }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Credit";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deleteCredit = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteCredit(id)
    .then((res) => {
      let CreditId = res.data;
      console.log(CreditId);
      dispatch(actions.CreditDeleted({ CreditId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete Credit";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createCredit = CreditForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createCredit(CreditForCreation)
    .then((res) => {
      let Credit = res.data;
      console.log(Credit);
      dispatch(actions.CreditCreated({ Credit }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Credit";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateCredit = (Credit,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Credit)
  return requestFromServer
    .updateCredit(Credit,id)
    .then((res) => {
      dispatch(actions.CreditUpdated({ Credit }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Credit";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateCreditPassword = (Credit,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Credit)
  return requestFromServer
    .updateCreditPassword(Credit,id)
    .then(() => {
      dispatch(actions.CreditUpdated({ Credit }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Credit";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateCreditsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForCredits(ids, status)
    .then(() => {
      dispatch(actions.CreditsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Credits status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteCredits = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteCredits(ids)
    .then(() => {
      dispatch(actions.CreditsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Credits";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
