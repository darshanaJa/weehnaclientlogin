import axios from "axios";
import { Api_Login } from "../../../config/config";

export const SalesTickets_URL = "api/SalesTickets";

export const SalesTickets_URL_FIND = Api_Login + '/api/sales-ticket/getall';
export const SalesTickets_URL_DELETE = Api_Login + '/api/sales-ticket/delete';
export const SalesTickets_URL_CREATE = Api_Login + '/api/sales-ticket/register';
export const SalesTickets_URL_UPDATE = Api_Login + '/api/sales-ticket/update';
export const SalesTickets_URL_GETBYID = Api_Login + '/api/sales-ticket/ticket-get';
export const SalesTickets_URL_GET = Api_Login + '';



// CREATE =>  POST: add a new SalesTicket to the server
export function createSalesTicket(SalesTicket) {
  return axios.post(SalesTickets_URL_CREATE, SalesTicket);
}

// READ
export function getAllSalesTickets() {
  return axios.get(SalesTickets_URL_GET);
}

export function getSalesTicketById(SalesTicketId) {
  return axios.get(`${SalesTickets_URL_GETBYID}/${SalesTicketId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findSalesTickets(queryParams) {
  console.log(queryParams.sales_ticket_id);
  if (queryParams.sales_ticket_id === null || queryParams.sales_ticket_id === "") {
  return axios.post(SalesTickets_URL_FIND , {
    "filter": [{"status": "1" }],
    "sort": "DESC",
    "limit": queryParams.pageSize, 
    "skip":(queryParams.pageNumber-1)*queryParams.pageSize
});
  } else {
    return axios.post(SalesTickets_URL_FIND , {
      "filter": [ {"sales_ticket_id" : queryParams.sales_ticket_id}],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
  }
}

// UPDATE => PUT: update the procuct on the server
export function updateSalesTicket(SalesTicket,id) {
  return axios.put(`${SalesTickets_URL_UPDATE}/${id}`, SalesTicket);
}

// UPDATE Status
export function updateStatusForSalesTickets(ids, status) {
  return axios.post(`${SalesTickets_URL}/updateStatusForSalesTickets`, {
    ids,
    status
  });
}

// DELETE => delete the SalesTicket from the server
export function deleteSalesTicket(SalesTicketId) {
  return axios.delete(`${SalesTickets_URL_DELETE}/${SalesTicketId}`);
}

// DELETE SalesTickets by ids
export function deleteSalesTickets(ids) {
  return axios.post(`${SalesTickets_URL}/deleteSalesTickets`, { ids });
}
