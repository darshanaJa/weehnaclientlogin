import axios from "axios";
import { Api_Login } from "../../../config/config";

// export const REMARKS_URL = "api/remarks";

export const REMARKS_URL = Api_Login + "/api/employee-history/search";
// export const Positions_URL_GET = Api_Login + "/api/main-stock/all?Page=1&limit=10";

// export const Positions_URL_CREATE = Api_Login + "/api/position/create";
// export const Positions_URL_DELETE = Api_Login + "/api/position/delete";
// export const Positions_URL_GETBYID = Api_Login + "/api/position";
// export const Positions_URL_UPDATE = Api_Login + "/api/position/update";

// CREATE =>  POST: add a new remark to the server
export function createRemark(remark) {
  // return axios.post(REMARKS_URL, { remark });
}

// READ
// Server should return filtered remarks by productId
export function getAllProductRemarksByProductId(productId) {
  // return axios.get(`${REMARKS_URL}?productId=${productId}`);
}

export function getRemarkById(remarkId) {
  // return axios.get(`${REMARKS_URL}/${remarkId}`);
}

// Server should return sorted/filtered remarks and merge with items from state
// TODO: Change your URL to REAL API, right now URL is 'api/remarksfind/{productId}'. Should be 'api/remarks/find/{productId}'!!!
export function findRemarks(queryParams, productId) {
  console.log(queryParams.position_name);
  if (queryParams.position_name === null || queryParams.position_name === "") {
    return axios.post(REMARKS_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(REMARKS_URL, { 
        "filter": [ {"position_name" : queryParams.position_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
}
}

// UPDATE => PUT: update the remark
export function updateRemark(remark) {
  // return axios.put(`${REMARKS_URL}/${remark.id}`, { remark });
}

// DELETE => delete the remark
export function deleteRemark(remarkId) {
  // return axios.delete(`${REMARKS_URL}/${remarkId}`);
}

// DELETE Remarks by ids
export function deleteRemarks(ids) {
  // return axios.post(`${REMARKS_URL}/deleteRemarks`, { ids });
}
