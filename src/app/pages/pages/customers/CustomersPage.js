import React from "react";
import { Route } from "react-router-dom";
import { CustomersLoadingDialog } from "./customers-loading-dialog/CustomersLoadingDialog";
// import { CustomerEditDialog } from "./customer-edit-dialog/CustomerEditDialog";
import { CustomerDeleteDialog } from "./customer-delete-dialog/CustomerDeleteDialog";
import { CustomersDeleteDialog } from "./customers-delete-dialog/CustomersDeleteDialog";
import { CustomersFetchDialog } from "./customers-fetch-dialog/CustomersFetchDialog";
import { CustomersUpdateStateDialog } from "./customers-update-status-dialog/CustomersUpdateStateDialog";
import { CustomersUIProvider } from "./CustomersUIContext";
import { CustomersCard } from "./CustomersCard";

export function CustomersPage({ history }) {
  const customersUIEvents = {
    newCustomerButtonClick: () => {
      history.push("/sales/payments/new");
    },
    openEditCustomerDialog: (id) => {
      history.push(`/sales/payments/${id}/edit`);
    },
    openDeleteCustomerDialog: (id) => {
      history.push(`/sales/payments/${id}/delete`);
    },
    openDeleteCustomersDialog: () => {
      history.push(`/sales/payments/deleteCustomers`);
    },
    openFetchCustomersDialog: () => {
      history.push(`/sales/payments/fetch`);
    },
    openUpdateCustomersStatusDialog: () => {
      history.push("/sales/payments/updateStatus");
    }
  }

  return (
    <CustomersUIProvider customersUIEvents={customersUIEvents}>
      <CustomersLoadingDialog />
      {/* <Route path="/sales/payments/new">
        {({ history, match }) => (
          <CustomerEditDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <Route path="/sales/payments/:id/edit">
        {({ history, match }) => (
          <CustomerEditDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route> */}
      <Route path="/sales/payments/deleteCustomers">
        {({ history, match }) => (
          <CustomersDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <Route path="/sales/payments/:id/delete">
        {({ history, match }) => (
          <CustomerDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <Route path="/sales/payments/fetch">
        {({ history, match }) => (
          <CustomersFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <Route path="/sales/payments/updateStatus">
        {({ history, match }) => (
          <CustomersUpdateStateDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <CustomersCard />
    </CustomersUIProvider>
  );
}
