import React, { useState, useEffect,
  //  useMemo 
  } from "react";
// import { Modal } from "react-bootstrap";
// import { shallowEqual, useDispatch, useSelector } from "react-redux";
// import * as actions from "../../../_redux/customers/customersActions";
// import { CustomerEditDialogHeader } from "./CustomerEditDialogHeader";
// import { CustomerEditForm } from "./CustomerEditForm";
// import { useCustomersUIContext } from "../CustomersUIContext";
import {useHistory} from "react-router-dom";
import {
  Card,
  CardBody,
  CardHeader,
  // CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { useParams } from "react-router-dom"
import { getCustomerById } from "../../../_redux/customers/customersCrud";
import { Image_Url } from "../../../../config/config";

export function CustomerEditDialog() {

  const [pdf, setpdf] = useState()

  const history = useHistory();
  const  params = useParams()
  console.log(params.id);

  useEffect(() => {
    getCustomerById(params.id)
    .then((res)=>{
      setpdf(res.data.file_url2)
    })
    }, [params.id])

    const file = Image_Url + pdf



  const backToCreditsList = () => {
    history.push(`/sales/payments`);
  };
  
  return (
    <Card>
    <CardHeader title="">
            <button
              style={{float:"right", height:"40px", marginTop:"15px"}}
              type="button"
              onClick={backToCreditsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
    </CardHeader>
    <CardBody>
      <iframe width="750px" height="650px" title="pdf1" src={file} />
    </CardBody>
  </Card>
  );
}
