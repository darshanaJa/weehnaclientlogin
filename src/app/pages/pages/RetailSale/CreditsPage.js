import React from "react";
import { Route } from "react-router-dom";
import { CreditsLoadingDialog } from "./Credits-loading-dialog/CreditsLoadingDialog";
import { CreditDeleteDialog } from "./Credit-delete-dialog/CreditDeleteDialog";
import { CreditsDeleteDialog } from "./Credits-delete-dialog/CreditsDeleteDialog";
import { CreditsFetchDialog } from "./Credits-fetch-dialog/CreditsFetchDialog";
import { CreditsUpdateStatusDialog } from "./Credits-update-status-dialog/CreditsUpdateStatusDialog";
import { CreditsCard } from "./CreditsCard";
import { CreditsUIProvider } from "./CreditsUIContext";

export function CreditsPage({ history }) {
  const CreditsUIEvents = {
    newCreditButtonClick: () => {
      history.push("/sales/history/new");
    },
    openEditCreditPage: (id) => {
      history.push(`/sales/history/${id}/edit`);
    },
    openDeleteCreditDialog: (sales_Credit_id) => {
      history.push(`/sales/history/${sales_Credit_id}/delete`);
    },
    openDeleteCreditsDialog: () => {
      history.push(`/sales/history/deleteCredits`);
    },
    openFetchCreditsDialog: () => {
      history.push(`/sales/history/fetch`);
    },
    openUpdateCreditsStatusDialog: () => {
      history.push("/sales/history/updateStatus");
    },
  };

  return (
    <CreditsUIProvider CreditsUIEvents={CreditsUIEvents}>
      <CreditsLoadingDialog />
      <Route path="/sales/history/deleteCredits">
        {({ history, match }) => (
          <CreditsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/history");
            }}
          />
        )}
      </Route>
      <Route path="/sales/history/:sales_Credit_id/delete">
        {({ history, match }) => (
          <CreditDeleteDialog
            show={match != null}
            sales_Credit_id={match && match.params.sales_Credit_id}
            onHide={() => {
              history.push("/sales/history");
            }}
          />
        )}
      </Route>
      <Route path="/sales/history/fetch">
        {({ history, match }) => (
          <CreditsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/history");
            }}
          />
        )}
      </Route>
      <Route path="/sales/history/updateStatus">
        {({ history, match }) => (
          <CreditsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/history");
            }}
          />
        )}
      </Route>
      <CreditsCard />
    </CreditsUIProvider>
  );
}
