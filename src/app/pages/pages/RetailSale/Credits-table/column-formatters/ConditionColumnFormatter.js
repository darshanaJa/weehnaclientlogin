import React from "react";
import {
  CreditConditionCssClasses,
  CreditConditionTitles
} from "../../CreditsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        CreditConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        CreditConditionCssClasses[row.condition]
      }`}
    >
      {CreditConditionTitles[row.condition]}
    </span>
  </>
);
