import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { CreditsFilter } from "./Credits-filter/CreditsFilter";
import { CreditsTable } from "./Credits-table/CreditsTable";
// import { CreditsGrouping } from "./Credits-grouping/CreditsGrouping";
import { useCreditsUIContext } from "./CreditsUIContext";
import { notify } from "../../../config/Toastify";

export function CreditsCard() {
  const CreditsUIContext = useCreditsUIContext();
  const CreditsUIProps = useMemo(() => {
    return {
      ids: CreditsUIContext.ids,
      queryParams: CreditsUIContext.queryParams,
      setQueryParams: CreditsUIContext.setQueryParams,
      newCreditButtonClick: CreditsUIContext.newCreditButtonClick,
      openDeleteCreditsDialog: CreditsUIContext.openDeleteCreditsDialog,
      openEditCreditPage: CreditsUIContext.openEditCreditPage,
      openUpdateCreditsStatusDialog:CreditsUIContext.openUpdateCreditsStatusDialog,
      openFetchCreditsDialog: CreditsUIContext.openFetchCreditsDialog,
    };
  }, [CreditsUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="Sale History">
        <CardHeaderToolbar>
        
          {/* <button
            type="button"
            className="btn btn-primary"
            onClick={CreditsUIProps.newCreditButtonClick}
          >
            New Retail Sale
          </button> */}
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <CreditsFilter />
        {CreditsUIProps.ids.length > 0 && (
          <>
            {/* <CreditsGrouping /> */}
          </>
        )}
        <CreditsTable />
      </CardBody>
    </Card>
  );
}
