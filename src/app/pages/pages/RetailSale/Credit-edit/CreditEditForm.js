import React, {useEffect,useState
  // , useReducer
} from "react";
// import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
// import { Input, Input2, Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
// import axios from "axios";
// import { Shops_URL } from "../../../_redux/shops/shopsCrud";
import { getCreditById } from "../../../_redux/Credit/CreditsCrud";
// import { getSalesTicketById, SalesTickets_URL_FIND } from "../../../_redux/salesTickets/salesTicketsCrud";
// import { getSpecificationById } from "../../../_redux/specificationsItem/specificationsCrud";
import { Image_Url } from "../../../../config/config";


export function CreditEditForm({id}) {

  const [pdf, setpdf] = useState()

  console.log(id)

  useEffect(() => {

  getCreditById(id)
  .then((res)=>{
    setpdf(res.data.file_url2)
  })

  }, [id])

  

  const file = Image_Url + pdf
  console.log(file);

  return (
    <>
      <iframe width="750px" title="pdf2" height="650px" src={file} />
    </>
  );
}
