import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { CreditstatusCssClasses } from "../CreditsUIHelpers";
import * as actions from "../../../_redux/Credit/CreditsActions";
import { useCreditsUIContext } from "../CreditsUIContext";

const selectedCredits = (entities, ids) => {
  const _Credits = [];
  ids.forEach((id) => {
    const Credit = entities.find((el) => el.id === id);
    if (Credit) {
      _Credits.push(Credit);
    }
  });
  return _Credits;
};

export function CreditsUpdateStatusDialog({ show, onHide }) {
  // Credits UI Context
  const CreditsUIContext = useCreditsUIContext();
  const CreditsUIProps = useMemo(() => {
    return {
      ids: CreditsUIContext.ids,
      setIds: CreditsUIContext.setIds,
      queryParams: CreditsUIContext.queryParams,
    };
  }, [CreditsUIContext]);

  // Credits Redux state
  const { Credits, isLoading } = useSelector(
    (state) => ({
      Credits: selectedCredits(state.Credits.entities, CreditsUIProps.ids),
      isLoading: state.Credits.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Credits we should close modal
  useEffect(() => {
    if (CreditsUIProps.ids || CreditsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [CreditsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Credit by ids
    dispatch(actions.updateCreditsStatus(CreditsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchCredits(CreditsUIProps.queryParams)).then(
          () => {
            // clear selections list
            CreditsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Credits
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Credits.map((Credit) => (
              <div className="list-timeline-item mb-3" key={Credit.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      CreditstatusCssClasses[Credit.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Credit.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Credit.manufacture}, {Credit.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${CreditstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
