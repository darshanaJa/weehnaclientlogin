import React, { useMemo } from "react";
import { useCreditsUIContext } from "../CreditsUIContext";

export function CreditsGrouping() {
  // Credits UI Context
  const CreditsUIContext = useCreditsUIContext();
  const CreditsUIProps = useMemo(() => {
    return {
      ids: CreditsUIContext.ids,
      setIds: CreditsUIContext.setIds,
      openDeleteCreditsDialog: CreditsUIContext.openDeleteCreditsDialog,
      openFetchCreditsDialog: CreditsUIContext.openFetchCreditsDialog,
      openUpdateCreditsStatusDialog:
        CreditsUIContext.openUpdateCreditsStatusDialog,
    };
  }, [CreditsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{CreditsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={CreditsUIProps.openDeleteCreditsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={CreditsUIProps.openFetchCreditsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={CreditsUIProps.openUpdateCreditsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
