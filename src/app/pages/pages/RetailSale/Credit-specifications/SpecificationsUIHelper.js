export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 },
];
export const initialFilter = {
  filter: {
    sales_ticket_item_id: "",
  },
  sales_ticket_item_id: null,
  sortOrder: "asc", // asc||desc
  sortField: "name",
  pageNumber: 1,
  pageSize: 10,
};

export const SPECIFICATIONS_DICTIONARY = [
  { id: 0, name: "Seats" },
  { id: 1, name: "Fuel Type" },
  { id: 2, name: "Stock" },
  { id: 3, name: "Door count" },
  { id: 4, name: "Engine" },
  { id: 5, name: "Transmission" },
  { id: 6, name: "Drivetrain" },
  { id: 7, name: "Combined MPG" },
  { id: 8, name: "Warranty" },
  { id: 9, name: "Wheels" },
];
